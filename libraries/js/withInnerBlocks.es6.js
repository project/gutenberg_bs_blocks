const { withSelect } = wp.data;

const getInnerBlocksProp = (select, ownProps) => {
    const { clientId } = ownProps;
    // Fallback to 'core/editor' for backwards compatibility
    const { getBlockOrder } =
    select("core/block-editor") || select("core/editor");
    return {
        hasInnerBlocks: getBlockOrder(clientId).length > 0,
    };
};
const withInnerBlocks = withSelect(getInnerBlocksProp);

export default withInnerBlocks;
